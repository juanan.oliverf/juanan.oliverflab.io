---
title: Don Quijote en el siglo XXI
date: 2020-04-24
---

Hoy he escrito un pequeño relato para Lengua y quería compartirlo en este blog.


# Don Quijote en nuestros días:

### “Los bandidos de San Jerónimo”

En un lugar de Madrid, de cuyo nombre no quiero acordarme, vivía un profesor
jubilado llamado Alonso Quijano, que enseñaba historia medieval en la
Universidad Juan Carlos I. El tal Quijano, tras jubilarse, se tiraba las mañanas y
las tardes leyendo libros de caballería de siglos atrás; hasta tal punto de
trasnochar para seguir leyendo dichos relatos.
Un día de marzo, el profesor se dijo:
- ¡Quizá debiera yo también hacerme caballero andante! Mi nombre de
honorable caballero será Don Quijote de la Mancha, ¡ y defenderé a los
campesinos de toda amenaza que puedan tener, como honorable
caballero que seré!

Incluso, cegado por la locura, llegó a pensar que tenía por esposa a una okupa
de un piso en el barrio de Vallecas por el nombre de Dulcinea del Manzanares,
imaginando el hedor y la suciedad por perfume y limpieza.
En tanto que Don Quijote, para comenzar sus andaduras de caballero, bajó
hasta la ciudad de Albacete a pie para comprar una espada y un escudo en la
herrería de la villa, aunque esta era en realidad una tienda de souvenirs para
turistas.

Y allí estaba, el completamente loco don Quijote, armado con espada, escudo y
armadura cabalgando por medio de la Puerta del Sol mientras los viandantes le
miraban desde lejos pensando:
-Ese hombre está completamente loco.

Don Quijote pasó hacia lo que se conoce como la carrera de San Jerónimo,
donde encontró a unos policías con sus porras y su equipamiento antidisturbios
desalojando a unos okupas de una vivienda. Y allí también estaba Dulcinea,
como él la llamaba, intentando impedir el desahucio.

Don Quijote, encolerizado, gritó a los agentes de la ley:
-¿Osáis, viles bandidos, asaltar a mi amada Dulcinea con vuestras espadas?
¡Como caballero que soy, os daré vuestro merecido!
Los policías se quedaron en shock cuando oyeron a aquel perturbado mental, y
más aún cuando, espada en ristre, el loco se lanzó a por ellos.
Esos segundos de shock le dieron la oportunidad a Don Quijote de derribar a
uno de los supuestos bandidos, antes de que un calambre sacudiese su cuerpo
y perdiera la consciencia, tras el disparo con la pistola eléctrica de otro de los
agentes.
Horas después, quizá incluso días, despertó Don Quijote y se encontraba en un
lugar extraño, encerrado en una habitación, sin recordar nada de lo que había
pasado anteriormente.

Entró una enfermera y le preguntó:
-¿Por fin despierta? Lleva varios días desmayado, señor Quijano. Bueno, o don
Quijote, como se hace llamar.
-¿Don Quijote?¿Qué tontería es esa?-respondió el profesor, extrañado.
-Pues lo de… Ah, ya veo que la medicación ha sido bastante efectiva.
Y así se quedó, el profesor, inocente de todo lo que había ocurrido, tomando su
medicación, por los años que le quedaban.
