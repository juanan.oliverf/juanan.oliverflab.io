---
title: Juegos de PC para la Cuarentena
date: 2020-04-17
---

#### Aquí traigo hoy una serie de videojuegos de ordenador para sobrellevar la cuarentena que estoy jugando 
#### en este período. 

## 1.- IMPERATOR : ROME

Este juego de estrategia de Paradox Interactive ambientado en la República romana es una de mis
opciones para la cuarentena. Puedes jugar con cualquier nación del momento y disfrutar de decenas
de horas de partida conquistando el mundo antiguo o fracasando en el intento. El juego está gratis 
en Steam durante los próximos días así que puedes aprovechar.

## 2.- EUROPA UNIVERSALIS IV

Es bastante parecido al anterior, si bien está ambientado en una época distinta: el juego comienza en el año 1444 d.C. y de nuevo, 
podrás jugar con cualquier nación. Podrás colonizar cualquier lugar de América o África sin importar el curso real de los
acontecimientos permitiéndote crear un universo alternativo a tu antojo, eso sí, siempre que sobrevivas.
El juego es sin embargo más caro, cuesta 39,99 € en Steam.

## 3.- FOOTBALL MANAGER 2020

Otro juego de estrategia, pero esta vez distinto. En este juego podrás asumir la dirección de cualquier equipo de fútbol de mundo
y llevarlo a la gloria. Te ofrce, como los anteriores, partidas bastante largas para llevar bien el confinamiento y si te 
gusta el fútbol, este es tu título. Eso sí, deberás instalar los escudos y nombres reales de los equipos de Internet puesto que
no están en el juego. Ahora mismo, el juego está disponible gratis en Steam.

## 4.- MOTORSPORT MANAGER

Parecido al anterior, pero esta vez en la Fórmula 1.Dirige un equipo real o crea el tuyo propio y compite en varias categorías de automovilismo 
dirigiendo a tus monoplazas carrera a carrera. Tiene muchas opciones de personalización del coche, como los patrocinadores. Muy recomendable.
